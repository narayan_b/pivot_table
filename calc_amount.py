import mysql.connector
import pandas as pd

mydb = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='pivot_table'
)

cursor = mydb.cursor()

def count_order(countries, cat, product):
    country = ', '.join(['%s'] * len(countries))
    catag = ', '.join(['%s'] * len(cat))
    pName = ', '.join(['%s'] * len(product))
    
    query = f"""
    SELECT c.country_name, p.product_name, SUM(o.amount)
    FROM country AS c
    JOIN `order` AS o ON o.country_id = c.country_id
    JOIN product AS p ON o.product_id = p.product_id
    WHERE c.country_name IN ({country})
    AND p.category IN ({catag})
    AND p.product_name IN ({pName})
    GROUP BY c.country_name, p.product_name;
    """
    
    cursor.execute(query, countries + cat + product)
    
    results = cursor.fetchall()

    df = pd.DataFrame(results, columns=['Country', 'Product', 'Sum of amount'])

    total_row = pd.DataFrame([['Total', 'Total', df['Sum of amount'].sum()]], columns=['Country', 'Product', 'Sum of amount'])
    df = pd.concat([df, total_row])

    pivot_table = df.pivot_table(values='Sum of amount', index='Product', columns='Country', aggfunc='sum')

    return pivot_table

if __name__ == '__main__':
    # print(count_order(["France"], ["Fruit"], ["Banana"]))
    print(count_order(["United States", "United Kingdom", "Canada"], ["Fruit", "Vegetable"], ["Banana", "Apple"]))
