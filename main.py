import pandas as pd
import openpyxl
import mysql.connector

xlsx = pd.ExcelFile("pivot-tables.xlsx")
df = pd.read_excel(xlsx, "Sheet1")

df_reset = df.set_index('Order ID')
df_reset['Category'] = df_reset['Category'].replace({'Vegetables': 'Vegetable'})
df_reset['Date'] = df_reset['Date'].apply(lambda x: str(x.date()) if isinstance(x, pd.Timestamp) else str(x))

print(df_reset)

mydb = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='pivot_table'
)

cursor = mydb.cursor()

for index, row in df_reset.iterrows():
    product_name = row["Product"]
    cursor.execute("SELECT product_id FROM product WHERE product_name = %s", (product_name,))
    existing_product = cursor.fetchone()
    if not existing_product:
        sql1 = "INSERT INTO product (product_name, category) VALUES (%s, %s)"
        val1 = (row["Product"], row["Category"])
        cursor.execute(sql1, val1)
        mydb.commit()
        existing_product = cursor.lastrowid
    else:
        existing_product = existing_product[0]

    country_name = row["Country"]
    cursor.execute("SELECT country_id FROM country WHERE country_name = %s", (country_name,))
    existing_country = cursor.fetchone()
    if not existing_country:
        sql2 = "INSERT INTO country (country_name) VALUES (%s)"
        val2 = (country_name,)
        cursor.execute(sql2, val2)
        mydb.commit()
        existing_country = cursor.lastrowid
    else:
        existing_country = existing_country[0]

    sql3 = "INSERT INTO `order` (product_id, country_id, date, amount) VALUES (%s, %s, %s, %s)" 
    val3 = (existing_product, existing_country, row["Date"], row["Amount"])
    cursor.execute(sql3, val3)
    mydb.commit()

print(cursor.rowcount, "record inserted.")

