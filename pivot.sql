DROP DATABASE IF EXISTS pivot_table;
CREATE DATABASE IF NOT EXISTS pivot_table;
USE pivot_table;

CREATE TABLE country (
    country_id INT AUTO_INCREMENT PRIMARY KEY,
    country_name VARCHAR(255)
);

CREATE TABLE product (
    product_id INT AUTO_INCREMENT PRIMARY KEY,
    product_name VARCHAR(255),
    category ENUM("Vegetable", "Fruit")
);

CREATE TABLE `order` (
    order_id INT AUTO_INCREMENT PRIMARY KEY,
    product_id INT REFERENCES product.product_id,
    country_id INT REFERENCES country.country_id,
    date DATE,
    amount VARCHAR(255)
);

GRANT ALL PRIVILEGES ON pivot_table.* TO 'narayan23'@'localhost';