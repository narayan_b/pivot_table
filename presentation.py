# import dash
# from dash import dcc, html
# from dash.dependencies import Input, Output
# import pandas as pd
# from calc_amount import count_order  # Import your function from your module

# countries_list = ["United States", "United Kingdom", "Canada", "Germany", "Australia", "New Zealand", "France"]
# categories_list = ["Fruit", "Vegetable"]
# products_list = ["Carrots", "Broccoli", "Banana", "Beans", "Orange", "Apple", "Mango"]

# app = dash.Dash(__name__)

# styles = {
#     'table': {
#         'borderCollapse': 'collapse',
#         'border': '1px solid white',
#         'textAlign': 'center'
#     },
#     'table-head': {
#         'backgroundColor': 'black',
#         'color': 'white'
#     },
#     'table-cell': {
#         'border': '1px solid white',
#         'padding': '8px'
#     }
# }

# app.layout = html.Div([
#     html.H1("Order Count Table"),
    
#     html.Label("Select Country:"),
#     dcc.Dropdown(
#         id='country-dropdown',
#         options=[{'label': country, 'value': country} for country in countries_list],
#         value=countries_list,
#         multi=True
#     ),
    
#     html.Label("Select Category:"),
#     dcc.Dropdown(
#         id='category-dropdown',
#         options=[{'label': category, 'value': category} for category in categories_list],
#         value=categories_list,
#         multi=True
#     ),
    
#     html.Label("Select Product:"),
#     dcc.Dropdown(
#         id='product-dropdown',
#         options=[{'label': product, 'value': product} for product in products_list],
#         value=products_list,
#         multi=True
#     ),
    
#     html.Div(id='output-table')
# ])

# @app.callback(
#     Output('output-table', 'children'),
#     [Input('country-dropdown', 'value'),
#      Input('category-dropdown', 'value'),
#      Input('product-dropdown', 'value')]
# )
# def update_table(selected_countries, selected_categories, selected_products):
#     if selected_countries and selected_categories and selected_products:
#         pivot_table = count_order(selected_countries, selected_categories, selected_products)
        
#         pivot_table = pivot_table.drop(columns=['Total'], errors='ignore')
#         pivot_table = pivot_table.loc[pivot_table.index != 'Total']
        
#         table_rows = []
        
#         header_row = [html.Th('Product', style=styles['table-cell'])]
#         for country in pivot_table.columns:
#             header_row.append(html.Th(country, style=styles['table-cell']))
#         table_rows.append(html.Tr(header_row, style=styles['table-head']))
        
#         for product, data in pivot_table.iterrows():
#             row = [html.Td(product, style=styles['table-cell'])]
#             for value in data:
#                 row.append(html.Td(str(value), style=styles['table-cell']))
#             table_rows.append(html.Tr(row))
        
#         return html.Table(table_rows, style=styles['table'])
#     else:
#         return "Please select at least one country, category, and product."


# if __name__ == '__main__':
#     app.run_server(debug=True)



import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import pandas as pd
from calc_amount import count_order
import plotly.express as px

countries_list = ["United States", "United Kingdom", "Canada", "Germany", "Australia", "New Zealand", "France"]
categories_list = ["Fruit", "Vegetable"]
products_list = ["Carrots", "Broccoli", "Banana", "Beans", "Orange", "Apple", "Mango"]

app = dash.Dash(__name__)

styles = {
    'table': {
        'borderCollapse': 'collapse',
        'border': '1px solid white',
        'textAlign': 'center'
    },
    'table-head': {
        'backgroundColor': 'black',
        'color': 'white'
    },
    'table-cell': {
        'border': '1px solid white',
        'padding': '8px'
    }
}

app.layout = html.Div([
    html.H1("Order Count Table and Histogram"),
    
    html.Label("Select Country:"),
    dcc.Dropdown(
        id='country-dropdown',
        options=[{'label': country, 'value': country} for country in countries_list],
        value=countries_list,
        multi=True
    ),
    
    html.Label("Select Category:"),
    dcc.Dropdown(
        id='category-dropdown',
        options=[{'label': category, 'value': category} for category in categories_list],
        value=categories_list,
        multi=True
    ),
    
    html.Label("Select Product:"),
    dcc.Dropdown(
        id='product-dropdown',
        options=[{'label': product, 'value': product} for product in products_list],
        value=products_list,
        multi=True
    ),
    
    html.Div(id='output-table'),
    
    dcc.Graph(id='product-histogram')  
])

@app.callback(
    [Output('output-table', 'children'),
     Output('product-histogram', 'figure')],
    [Input('country-dropdown', 'value'),
     Input('category-dropdown', 'value'),
     Input('product-dropdown', 'value')]
)
def update_table_and_histogram(selected_countries, selected_categories, selected_products):
    if selected_countries and selected_categories and selected_products:
        pivot_table = count_order(selected_countries, selected_categories, selected_products)
        
        pivot_table = pivot_table.drop(columns=['Total'], errors='ignore')
        pivot_table = pivot_table.loc[pivot_table.index != 'Total']
        
        data = pivot_table.stack().reset_index()
        data.columns = ['Product', 'Country', 'Amount']
        
        fig = px.bar(data, x='Product', y='Amount', color='Country', barmode='group')
        fig.update_layout(xaxis_title='Product', yaxis_title='Sum of Amount')
        
        table_rows = []
        header_row = [html.Th('Product', style=styles['table-cell'])]
        for country in pivot_table.columns:
            header_row.append(html.Th(country, style=styles['table-cell']))
        table_rows.append(html.Tr(header_row, style=styles['table-head']))
        for product, data in pivot_table.iterrows():
            row = [html.Td(product, style=styles['table-cell'])]
            for value in data:
                row.append(html.Td(str(value), style=styles['table-cell']))
            table_rows.append(html.Tr(row))
        
        return html.Table(table_rows, style=styles['table']), fig
    else:
        return "Please select at least one country, category, and product.", {}


if __name__ == '__main__':
    app.run_server(debug=True)
