import mysql.connector
import pandas as pd

mydb = mysql.connector.connect(
    host="localhost",
    user="narayan23",
    password="coucou123",
    auth_plugin='mysql_native_password',
    database='pivot_table'
)

cursor = mydb.cursor()

def count_order(countries):
    country = list(countries)
    var = ', '.join(['%s'] * len(country))
    
    query = f"""
    SELECT p.product_name, COUNT(o.order_id)
    FROM country AS c
    JOIN `order` AS o ON o.country_id = c.country_id
    JOIN product AS p ON o.product_id = p.product_id
    WHERE c.country_name IN ({var})
    GROUP BY p.product_name;
    """
    
    cursor.execute(query, countries)
    
    results = cursor.fetchall()

    df = pd.DataFrame(results, columns=['Product', 'Count of amount'])

    total_row = pd.DataFrame([['Total', df['Count of amount'].sum()]], columns=['Product', 'Count of amount'])
    df = pd.concat([df, total_row])

    pivot_table = df.pivot_table(values='Count of amount', index='Product', columns=None, aggfunc='sum')

    return pivot_table
    


# results = count_order("France")
# results = count_order("United States", "United Kingdom", "Canada", "Germany", "Australia", "New Zealand", "France")


print(count_order(["France"]))
# print(count_order("United States", "United Kingdom", "Canada", "Germany", "Australia", "New Zealand", "France"))
